import 'antd/dist/antd.css'
import 'travelcloud-antd/styles-default.css'
import React, { Component } from 'react';
import { Row, Col, Menu, Carousel, Tabs, Button, Icon, Card, Avatar } from 'antd'
import { ToursResult } from 'travelcloud-antd/components/tours-result'
import { TravelCloudClient, updateFormState, Cart, extractValueFromFormState } from 'travelcloud-antd'
import { TourBookingForm, initialTourBookingForm } from 'travelcloud-antd/components/tour-booking'
import { CheckoutForm } from 'travelcloud-antd/components/checkout'
import { Order } from 'travelcloud-antd/components/order'
import { Account } from 'travelcloud-antd/components/account'
import  Big from 'big.js'

const TabPane = Tabs.TabPane

class App extends Component {
  formRef

  client = new TravelCloudClient('edge2')

  state = {
    cart: null,
    tabKey: "Search",
    'categories.name': '11D',
    tours: {},
    categories: {},
    tour: {},
    cartOrder: {},
    serverOrder: {},
    serverError: false,
    customer: {},
    tourBookingForm: null
  }

  async componentDidMount() {
    this.setState({loading: true})

    const tours = await this.client.tours({})
    const categories = await this.client.categories({category_type: 'tour_package'})

    this.setState({
      tours,
      categories,
      loading: false,
      cart:  new Cart({
        client: this.client,
          onOrderChange: (order) => this.setState({cartOrder: order}),
          onCustomerChange: (customer) => this.setState({customer})}
      )})
  }

  onSubmit (e) {
    e.preventDefault();
    this.formRef.props.form.validateFieldsAndScroll(async (error, values) => {
      if (!error) {
        this.setState({
          submittingOrder: true
        })

        const result = await this.state.cart.checkout(values)

        if (result.result == null) {
          this.setState({
            serverError: true,
            tabKey: "Payment"
          })
        } else {
          this.setState({
            serverError: false,
            serverOrder: result,
            tabKey: "Payment"
          })
        }
      } else {
        console.log('error', error, values);
      }
    });
  };

  async payWithAccountCredit() {
    this.setState({
      submittingPayment: true,
    })
    const ref = this.state.serverOrder.result.ref
    await this.state.cart.payOrderWithCustomerCredit(ref)
    const result = await this.client.order({ref})
    this.setState({
      submittingPayment: false,
      serverOrder: result
    })
  }

  render() {
    const tour = this.state.tour.result
    const customer = this.state.customer.result
    const bigAccountBalance = customer == null ? Big(0) : this.state.customer.result.payments.reduce((acc, payment) => acc.plus(payment.amount), Big(0)).times(-1)
    return (
      <div className="App" style={{margin: '20px'}}>
        <Tabs activeKey={this.state.tabKey} onChange={(key) => this.setState({tabKey: key})}>
          <TabPane tab="Login" key="Login">
            <Account customer={this.state.customer} cart={this.state.cart} />
          </TabPane>
          <TabPane tab="Search" key="Search">
            <Row gutter={32}>
              <Col span={6}>
                <h1>Tours</h1>
                <Menu
                  selectedKeys={[this.state['categories.name']]}
                  onClick={async ({key}) => {
                    this.setState({'tours': {}, 'categories.name' : key})

                    const tours = await this.client.tours({'categories.name' : key})

                    this.setState({tours, loading: false})
                  }}
                  mode="vertical">
                  {this.state.categories.result != null
                    &&this.state.categories.result.map(category =>
                      <Menu.Item key={category.name}>
                        <span>{category.name}</span>
                      </Menu.Item>)}
                </Menu>
              </Col>
              <Col span={18}>
                {this.state.tours.result != null
                  && <ToursResult
                        cart={this.state.cart}
                        toursResult={this.state.tours.result}
                        onTourClick={async (tour) => {
                          this.setState({tour: {}, tourBookingForm: initialTourBookingForm(tour.id)})
                          const tourRsp = await this.client.tour({id : tour.id})
                          this.setState({tour: tourRsp, tabKey: "Details"})
                        }} />}
              </Col>
            </Row>
          </TabPane>
          {tour != null && <TabPane tab="Details" key="Details">
            <Row>
              <Col>
                <Row style={{margin: '32px 64px'}}>
                  <h1 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h1>
                </Row>
                <Row style={{backgroundColor: '#fff', margin: '32px 64px'}}>
                  <Col span={12}>
                    <Carousel autoplay>
                      <div key='cover'><div style={{
                        backgroundImage: `url(${tour.photo_url})`,
                        height: '600px',
                        width: '100%',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'}} /></div>
                      {tour.photos.map((photo, index) =>
                        <div key={index}><div style={{
                          backgroundImage: `url(${photo.url})`,
                          height: '600px',
                          width: '100%',
                          backgroundSize: 'cover',
                          backgroundPosition: 'center'}} /></div>)}
                    </Carousel>
                  </Col>
                  <Col span={12} style={{height: '100%'}}>
                    <TourBookingForm value={this.state.tourBookingForm} tour={tour} onChange={updateFormState(this, 'tourBookingForm')}
                      cart={this.state.cart}
                      onSubmit={(e) => {
                        e.preventDefault()
                        const value = extractValueFromFormState(this.state.tourBookingForm)
                        this.state.cart.reset().addTour(tour, value)
                        this.setState({tabKey: 'Checkout'})
                      }} />
                  </Col>
                </Row>
                <Row gutter={64} style={{backgroundColor: '#fff', margin: '32px 64px', padding: 32}}>
                  <Col span={12}>
                    <h1>Highlights</h1>
                    <div dangerouslySetInnerHTML={{__html: tour.short_desc}} />
                    <h1>Extras</h1>
                    <div dangerouslySetInnerHTML={{__html: tour.extras}} />
                    <h1>Remarks</h1>
                    <div dangerouslySetInnerHTML={{__html: tour.remarks}} />
                  </Col>
                  <Col span={12}>
                    <h1>Itinerary</h1>
                    <div dangerouslySetInnerHTML={{__html: tour.itinerary}} />
                  </Col>
                </Row>
              </Col>
            </Row>
          </TabPane>}

          {this.state.cartOrder.result != null && <TabPane tab="Checkout" key="Checkout">
            <Row>
              <Col span={12}>
                <h1>Contact Details</h1>
                <CheckoutForm
                  order={this.state.cartOrder.result}
                  wrappedComponentRef={(inst) => this.formRef = inst}
                  demo={true} />
                  <Button size="large" type="primary" disabled={this.state.submittingOrder} onClick={(e) => this.onSubmit(e)}>Proceed to payment <Icon type={this.state.submittingOrder ? 'loading' : 'right'} /></Button>
              </Col>
              <Col span={12}>
                <h1>Order Details</h1>
                <Order order={this.state.cartOrder.result} showSection={{products: true}} />
              </Col>
            </Row>
          </TabPane>}

          {(this.state.serverError === true || this.state.serverOrder.result != null) && <TabPane tab="Payment" key="Payment">
            <Row>
              { this.state.serverError === true
                ? <Col><h1>We were not able to process your booking online</h1></Col>
                : <React.Fragment>
                    {this.state.serverOrder.result.payment_required === this.state.serverOrder.result.payment_received
                      ? <Col span={12}>Thank you for your payment!</Col>
                      : (customer != null
                        ? <Col span={12} style={{height: 114, padding: 32, border: "1px #ddd solid", borderRadius: 5, position: 'relative', marginTop: 32}}>
                            <Card.Meta
                              avatar={<Avatar size="large" icon="user" style={{backgroundColor: "#af0201", marginLeft: 16}} />}
                              title={customer.name
                                  ? customer.name
                                  : customer.email}
                              description={<span>Account balance ${bigAccountBalance.toFixed(2)}</span>} />
                              <Button disabled={bigAccountBalance.lt(this.state.serverOrder.result.payment_required) || this.state.submittingPayment} style={{position: 'absolute', top: 36, right: 24, width: 160}} type="primary"  onClick={() => this.payWithAccountCredit()}>Account Credit <Icon type="right" /></Button>
                          </Col>
                        : <Col span={12}>Please login to pay with account credit</Col>)
                    }
                    <Col span={12}><Order order={this.state.serverOrder.result} /></Col>
                  </React.Fragment>
              }
            </Row>
          </TabPane>}
        </Tabs>
      </div>
    );
  }
}

export default App;
